class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None

class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        self.raiz = self._insertar_recursivo(self.raiz, elemento)

    def _insertar_recursivo(self, nodo, elemento):
        if nodo is None:
            return Nodo(elemento)
        if elemento < nodo.valor:
            nodo.izquierda = self._insertar_recursivo(nodo.izquierda, elemento)
        elif elemento > nodo.valor:
            nodo.derecha = self._insertar_recursivo(nodo.derecha, elemento)
        return nodo

    def eliminar(self, elemento):
        self.raiz = self._eliminar_recursivo(self.raiz, elemento)
     
    def _eliminar_recursivo(self, nodo, elemento):
        if nodo is None:
            return None
        if elemento < nodo.valor:
            nodo.izquierda = self._eliminar_recursivo(nodo.izquierda, elemento)
        elif elemento > nodo.valor:
            nodo.derecha = self._eliminar_recursivo(nodo.derecha, elemento)
        else:
            if nodo.izquierda is None and nodo.derecha is None:
                return None
            if nodo.izquierda is None:
                return nodo.derecha
            if nodo.derecha is None:
                return nodo.izquierda
            minimo = self._minimo_recursivo(nodo.derecha)
            nodo.valor = minimo 
            nodo.derecha = self._eliminar_recursivo(nodo.derecha, minimo)
        return nodo
   
    def existe(self, elemento):
        return self._existe_recursivo(self.raiz, elemento)
    
    def _existe_recursivo(self, nodo, elemento):
        if nodo is None:
            return False
        if elemento < nodo.valor:
            return self._existe_recursivo(nodo.izquierda, elemento)
        elif elemento > nodo.valor:
            return self._existe_recursivo(nodo.derecha, elemento)
        else:
            return True      

    def maximo(self):
        return self._maximo_recursivo(self.raiz)

    def _maximo_recursivo(self,nodo):
        if nodo is None:
            return None
        if nodo.derecha is None:
            return nodo.valor
        return self._maximo_recursivo(nodo.derecha)
        
    def minimo(self):
        return self._minimo_recursivo(self.raiz)

    def _minimo_recursivo(self,nodo):
        if nodo is None:
            return None
        if nodo.izquierda is None:
            return nodo.valor
        return self._minimo_recursivo(nodo.izquierda)

    def altura(self):
        return self._altura_recursivo(self.raiz)
   
    def _altura_recursivo(self, nodo):
        if nodo is None:
            return 0
        altura_izquierda = self._altura_recursivo(nodo.izquierda)
        altura_derecha = self._altura_recursivo(nodo.derecha)
        return max(altura_izquierda, altura_derecha) + 1
            
    def obtener_elementos(self):
        elementos = []
        self._obtener_elementos_recursivo(self.raiz, elementos)
        return elementos
    
    def _obtener_elementos_recursivo(self, nodo, elementos):
        if nodo is None:
            return
        self._obtener_elementos_recursivo(nodo.izquierda, elementos)
        elementos.append(nodo.valor)
        self._obtener_elementos_recursivo(nodo.derecha, elementos)


    def __str__(self):
        lista = self._str_recursivo(self.raiz, []) 
        cadena = "[ " + " ".join(map(str, lista)) + " ]"
        return cadena

    def _str_recursivo(self, nodo, lista):
        if nodo is not None:
           lista = self._str_recursivo(nodo.izquierda, lista)
           lista.append(nodo.valor)
           lista = self._str_recursivo(nodo.derecha, lista)
        return lista